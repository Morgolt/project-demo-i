#!/bin/bash

APP_USER='app_user'
sudo useradd -m -s /bin/bash $APP_USER

sudo apt-get update
sudo apt-get install -y openjdk-11-jdk git

sudo -u $APP_USER mkdir -p /home/$APP_USER
sudo chown -R $APP_USER:$APP_USER /home/$APP_USER

# Клонування репозиторію, який я попередньо клонував на власну сторінку
PROJECT_DIR="/home/$APP_USER/petclinic"
REPO_URL="https://gitlab.com/Morgolt/stepproject.git"
sudo -u $APP_USER git clone $REPO_URL $PROJECT_DIR

cd $PROJECT_DIR/StepProjects/PetClinic || exit

cd $PROJECT_DIR/StepProjects/PetClinic || exit
sudo chmod +x mvnw
sudo -u $APP_USER ./mvnw clean package

APP_DIR="/home/$APP_USER/app"
sudo -u $APP_USER mkdir -p $APP_DIR
sudo -u $APP_USER cp $PROJECT_DIR/StepProjects/PetClinic/target/*.jar $APP_DIR/

echo "export DB_HOST=192.168.56.10" | sudo tee -a /home/$APP_USER/.bashrc
echo "export DB_PORT=3306" | sudo tee -a /home/$APP_USER/.bashrc
echo "export DB_NAME=petclinic" | sudo tee -a /home/$APP_USER/.bashrc
echo "export DB_USER=pc_user" | sudo tee -a /home/$APP_USER/.bashrc
echo "export DB_PASS=pc_pass" | sudo tee -a /home/$APP_USER/.bashrc

sudo -u $APP_USER bash -c "source /home/$APP_USER/.bashrc && java -jar $APP_DIR/*.jar > /home/$APP_USER/app.log 2>&1 &"

